/*
 * Job_test.cpp
 *
 *  Created on: 9 Oct 2016
 *      Author: Wessel
 */

#include "Job.hpp"

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( Job_Test )

BOOST_AUTO_TEST_CASE( JobNotFilledOnStart )
{
	Job j1(0);

	BOOST_REQUIRE(!j1.isBusy());
	BOOST_REQUIRE_EQUAL(-1, j1.getStartTime());
	BOOST_REQUIRE_EQUAL(0, j1.getEndTime());
	BOOST_REQUIRE(!j1.hasTasks());
}

BOOST_AUTO_TEST_CASE( JobHandlesNewTasksCorrectly )
{
	Job j1(0);
	j1.addTask(0, 45);
	j1.addTask(3, 13);

	BOOST_REQUIRE_EQUAL(2, j1.getTaskList().size());

	BOOST_REQUIRE_EQUAL(58, j1.getDuration());

	j1.addTask(4, 88);
	j1.addTask(2, 54);

	BOOST_REQUIRE_EQUAL(4, j1.getTaskList().size());

	BOOST_REQUIRE_EQUAL(200, j1.getDuration());

	j1.finishTask();

	BOOST_REQUIRE_EQUAL(3, j1.getTaskList().size());

	BOOST_REQUIRE_EQUAL(155, j1.getDuration());
}

BOOST_AUTO_TEST_CASE( JobHasCorrectSlack )
{
	Job j1(0);
	j1.addTask(0, 2);
	j1.addTask(1, 5);
	j1.addTask(0, 9);

	BOOST_REQUIRE_EQUAL(4, j1.calculateSlack(20));
}

BOOST_AUTO_TEST_CASE( JobAssignmentOperator )
{
	Job j1(0);
	j1.addTask(0, 13);
	j1.addTask(1, 3);
	j1.addTask(2, 9);

	Job j2(1);
	j2.addTask(2, 46);
	j2.addTask(1, 5);
	j2.addTask(0, 27);
	j2.addTask(0, 4);

	BOOST_REQUIRE_NE(j1.getDuration(), j2.getDuration());

	j2 = j1;

	BOOST_REQUIRE_EQUAL(j1.isBusy(), j2.isBusy());
	BOOST_REQUIRE_EQUAL(j1.getTaskList().size(), j2.getTaskList().size());
	BOOST_REQUIRE_EQUAL(j1.getDuration(), j2.getDuration());
	BOOST_REQUIRE_EQUAL(j1.calculateSlack(7), j2.calculateSlack(7));
	BOOST_REQUIRE_EQUAL(j1.getFirstTask().getTime(),
			j2.getFirstTask().getTime());
}

BOOST_AUTO_TEST_CASE( JobCopyConstructor )
{
	Job j1(0);

	j1.addTask(0, 1);
	j1.addTask(1, 4);
	j1.addTask(2, 2);

	j1.startTask();

	Job j2(j1);

	BOOST_REQUIRE_EQUAL(j1.isBusy(), j2.isBusy());
	BOOST_REQUIRE_EQUAL(j1.getTaskList().size(), j2.getTaskList().size());
	BOOST_REQUIRE_EQUAL(j1.getDuration(), j2.getDuration());
	BOOST_REQUIRE_EQUAL(j1.calculateSlack(7), j2.calculateSlack(7));
}

BOOST_AUTO_TEST_SUITE_END()
