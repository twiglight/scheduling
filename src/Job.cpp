/**
 * @file Job.cpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#include "Job.hpp"

Job::Job() :
		id(0), busy(false), duration(0), startTime(-1), endTime(0)
{
}

Job::Job(unsigned long aId) :
		id(aId), busy(false), duration(0), startTime(-1), endTime(0)
{
}

Job::Job(const Job& aJob) :
		id(aJob.id), taskList(aJob.taskList), busy(aJob.busy), duration(
				aJob.duration), startTime(aJob.startTime), endTime(aJob.endTime)
{
}

Job::~Job()
{
}

const Job& Job::operator =(const Job& aJob)
{
	if (this != &aJob)
	{
		id = aJob.id;
		taskList = aJob.taskList;
		busy = aJob.busy;
		duration = aJob.duration;
		startTime = aJob.startTime;
		endTime = aJob.endTime;
	}

	return *this;
}

void Job::addTask(unsigned long aMachine, unsigned long aTime)
{

	Task aTask(taskList.size(), aMachine, aTime);
	taskList.push_back(aTask);
	duration += aTask.getTime();
}

void Job::startTask()
{
	busy = true;
}

void Job::finishTask()
{
	duration -= taskList.front().getTime();
	taskList.erase(taskList.begin());

	busy = false;
}

unsigned long Job::calculateSlack(unsigned long aCriticalPath) const
{
	if (duration == 0)
	{
		return 0;
	}

	unsigned long es = duration - taskList.back().getTime();
	unsigned long ls = aCriticalPath - taskList.back().getTime();
	return (ls - es);
}

const Task& Job::getFirstTask() const
{
	return taskList.front();
}

unsigned long Job::getId() const
{
	return id;
}

void Job::setId(unsigned long aId)
{
	id = aId;
}

bool Job::hasTasks()
{
	return !taskList.empty();
}

const std::vector<Task>& Job::getTaskList() const
{
	return taskList;
}

void Job::setTaskList(const std::vector<Task>& aTaskList)
{
	taskList = aTaskList;
}

bool Job::isBusy() const
{
	return busy;
}

unsigned long long Job::getDuration() const
{
	return duration;
}

signed long long Job::getStartTime() const
{
	return startTime;
}

void Job::setStartTime(signed long long aStartTime)
{
	startTime = aStartTime;
}

unsigned long long Job::getEndTime() const
{
	return endTime;
}

void Job::setEndTime(unsigned long long anEndTime)
{
	endTime = anEndTime;
}
