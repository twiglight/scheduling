/*
 * JobShop_test.cpp
 *
 *  Created on: 9 okt. 2016
 *      Author: Agit
 */


#include "JobShop.hpp"

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
BOOST_AUTO_TEST_SUITE(JobShop_Test)


BOOST_AUTO_TEST_CASE(readFile_InvalidFile)
{
	JobShop a;
    std::string file = "invalidFileName";
    BOOST_CHECK_THROW(a.readFile(file.c_str()) , std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(fillJobList_checkSize)
{
	JobShop a;
	BOOST_REQUIRE_EQUAL(0,a.getJobList().size());
	BOOST_REQUIRE_EQUAL(0,a.getMachineList().size());

	std::vector<std::string> list;
	list.push_back("6 6");
	list.push_back("2  1  0  3  1  6  3  7  5  3  4  6");
	list.push_back("1  8  2  5  4 10  5 10  0 10  3  4");
	list.push_back("2  5  3  4  5  8  0  9  1  1  4  7");
	list.push_back("1  5  0  5  2  5  3  3  4  8  5  9");
	list.push_back("2  9  1  3  4  5  5  4  0  3  3  1");
	list.push_back("1  3  3  3  5  9  0 10  4  4  2  1");

	a.fillJobList(list);
	BOOST_REQUIRE_EQUAL(6,a.getJobList().size());
	BOOST_REQUIRE_EQUAL(6,a.getMachineList().size());
}


BOOST_AUTO_TEST_SUITE_END()


