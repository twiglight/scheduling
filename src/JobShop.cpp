/**
 * @file JobShop.cpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#include <fstream>
#include <regex>
#include <algorithm>
#include <exception>
#include "JobShop.hpp"

JobShop::JobShop() :
		currentTime(0)
{
}

JobShop::~JobShop()
{
}

std::vector<std::string> JobShop::readFile(const char* aFileName)
{
	std::ifstream file(aFileName);
	std::string line;
	std::vector<std::string> content;

	if (file.is_open())
	{
		while (getline(file, line))
		{
			content.push_back(line);
		}
		file.close();
	}
	else
	{
		throw std::invalid_argument
		{ "given file could not be opened or does not exist" };
	}
	return content;
}

void JobShop::fillJobList(std::vector<std::string> aFile)
{
	unsigned long nJobs = 0;
	unsigned long nMachines = 0;

	std::regex e("([[:digit:]]+)[[:space:]]+([[:digit:]]+)");
	std::smatch matchResults;

	try
	{
		for (unsigned long i = 0; i < aFile.size(); ++i)
		{
			while (std::regex_search(aFile.at(i), matchResults, e))
			{
				if (i == 0)
				{
					nJobs = stoul(matchResults.str(1)); //cast first submatch to integer
					if (aFile.size() - 1 < nJobs)
						throw std::length_error
						{ "Your are missing "
								+ std::to_string(nJobs - (aFile.size() - 1))
								+ " job(s) in your file" }; //throw how much jobs we are missing
					nMachines = stoul(matchResults.str(2)); //cast second submatch to integer

					for (unsigned long j = 0; j < nJobs; ++j)
					{
						jobList.push_back(Job(j));
					}

					break;
				}
				else
				{
					jobList.at(i - 1).addTask(stoul(matchResults.str(1)),
							stoul(matchResults.str(2)));
					createMachine(stoul(matchResults.str(1)));
					aFile.at(i) = matchResults.suffix();

				}
			}
		}

		if (nJobs == 0)
			throw std::length_error
			{ "No jobs defined, number must be greater than 0" };
		if (nMachines == 0)
			throw std::length_error
			{ "No machines defined, number must be greater than 0" };
		if (machineList.size() > nMachines)
			throw std::length_error
			{ "You have " + std::to_string(machineList.size() - nMachines)
					+ " more machines than defined" };
	} catch (std::out_of_range& e)
	{
		throw std::out_of_range
		{ "there are more jobs than there is defined in the first line" };
	} catch (std::invalid_argument& e)
	{
		throw std::invalid_argument
		{ "here come dat boi" };
	}
}

void JobShop::scheduleJobs()
{
	unsigned long ciriticalPath = 0;

	while ((ciriticalPath = calculateCriticalPath()) > 0)
	{

		scheduleTasksInMachines(ciriticalPath);
		handleTasksInMachines();
	}

	sortJobs();
}

const std::vector<Job>& JobShop::getJobList() const
{
	return jobList;
}

unsigned long JobShop::calculateCriticalPath()
{
	unsigned long ciriticalPath = 0;
	for (auto &j : jobList)
	{
		if (j.getDuration() > ciriticalPath)
		{
			ciriticalPath = j.getDuration();
		}
	}

	return ciriticalPath;
}

void JobShop::createMachine(unsigned long aMachineId)
{
	auto result = find_if(machineList.begin(), machineList.end(),
			[aMachineId](const Machine& aMachine)
			{	return aMachine.getId()==aMachineId;});

	if (result == machineList.end())
	{
		machineList.push_back(Machine(aMachineId));
	}
}

void JobShop::scheduleTasksInMachines(unsigned long aCiriticalPath)
{
	sort(jobList.begin(), jobList.end(),
			[aCiriticalPath](const Job& lhs,const Job& rhs)
			{	unsigned long lhsSlack = lhs.calculateSlack(aCiriticalPath);
				unsigned long rhsSlack = rhs.calculateSlack(aCiriticalPath);
				if(lhsSlack==rhsSlack)
				{
					return lhs.getId()<rhs.getId();
				}
				return lhsSlack<rhsSlack;});

	for (Job& j : jobList)
	{
		if (!j.isBusy() && j.hasTasks())
		{
			auto result =
					find_if(machineList.begin(), machineList.end(),
							[&j](const Machine& aMachine)
							{	return (j.getFirstTask().getMachine()==aMachine.getId()) && aMachine.isFree();});

			if (result != machineList.end())
			{
				(*result).setTask(j.getFirstTask().getTime(), j.getId());
				if (j.getStartTime() < 0)
				{
					j.setStartTime(currentTime);
				}
			}
		}
	}
}

void JobShop::handleTasksInMachines()
{
	sort(machineList.begin(), machineList.end(),
			[](const Machine& lhs,const Machine& rhs)
			{	if(lhs.isFree()) return false;
				else if(rhs.isFree()) return true;
				return (lhs.getTime()<rhs.getTime());
			});

	currentTime += machineList.front().getTime();
	unsigned long decrementTime = machineList.front().getTime();

	for (auto &m : machineList)
	{
		if (!m.isFree())
		{
			m.decrementTime(decrementTime);
			if (m.isFree())
			{
				auto result = find_if(jobList.begin(), jobList.end(),
						[&m](const Job& aJob)
						{	return m.getJobId()==aJob.getId();});

				if (result != jobList.end())
				{
					(*result).finishTask();

					if ((*result).getTaskList().empty())
					{
						(*result).setEndTime(currentTime);
					}
				}
			}
		}
	}
}

void JobShop::sortJobs()
{
	sort(jobList.begin(), jobList.end(), [](const Job& jobA,const Job& jobB)
	{	return (jobA.getId()<jobB.getId());});
}

std::ostream& operator <<(std::ostream& os, const JobShop& aJobShop)
{
	for (auto &j : aJobShop.getJobList())
	{
		os << j.getId() << "\t" << j.getStartTime() << "\t" << j.getEndTime()
				<< std::endl;
	}
	return os;
}

const std::vector<Machine>& JobShop::getMachineList() const
{
	return machineList;
}
