/**
 * @file main.cpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#include <exception>
#include "JobShop.hpp"

#define BOOST_TEST_MODULE SchedulingTestModule
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_NO_MAIN
#include <boost/test/unit_test.hpp>

int main(int argc, char* argv[])
{
	if (argc >= 2)
	{
		try
		{
			JobShop a;
			a.fillJobList(a.readFile(argv[1]));
			a.scheduleJobs();
			std::cout << a << std::endl;

		} catch (std::exception& e)
		{
			std::cerr << "Exeception: " << e.what() << std::endl;
		}

		return 0;
	}
	else
	{
		std::cout << "Running unit tests" << std::endl;
		return boost::unit_test::unit_test_main(&init_unit_test, argc, argv);
	}

}

