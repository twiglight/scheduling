/**
 * @file Job.hpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#ifndef JOB_HPP_
#define JOB_HPP_

#include <vector>
#include <iostream>
#include "Task.hpp"

/**
 * @brief A representation of a job which needs to be scheduled to be completed.
 *
 * This class contains and controls the tasks which will need to be executed by machines
 */
class Job
{
public:
	Job();

	/**
	 * @brief A constructor which creates a Job with a given id
	 * @param aId The id the Job should use
	 */
	Job(unsigned long aId);

	Job(const Job& aJob);

	virtual ~Job();

	const Job& operator=(const Job& aJob);

	/**
	 * @brief Adds a task to the #taskList
	 *
	 * A task is created with the given machine and time and added to the #taskList.
	 * The amount of time the task will need is added to #duration.
	 *
	 * @param aMachine the machine id for the new task
	 * @param aTime the time the new task will need
	 *
	 * @see Task#Task(unsigned long aId,unsigned long aMachine, unsigned long aTime)
	 */
	void addTask(unsigned long aMachine, unsigned long aTime);

	/**
	 * @brief Sets #busy on true
	 */
	void startTask();

	/**
	 * @brief Sets #busy on false
	 */
	void finishTask();

	/**
	 * @brief Calculates the slack of the first task.
	 *
	 * This function actually calculates the slack of the last element in #taskList, but
	 * the slack is the same for every task per Job.
	 * The slack is calculated by first calculating the earliest possible start time (es)
	 * and the latest possible start time (ls). Afterwards the es is subtracted from the
	 * ls which results in the slack for every task in this job.
	 *
	 * @param aCriticalPath the duration of the longest Job, which is used to calculate the ls
	 *
	 * @returns The calculated slack
	 */
	unsigned long calculateSlack(unsigned long aCriticalPath) const;

	const Task& getFirstTask() const;

	unsigned long getId() const;
	void setId(unsigned long id);
	bool hasTasks();
	const std::vector<Task>& getTaskList() const;
	void setTaskList(const std::vector<Task>& taskList);
	bool isBusy() const;
	unsigned long long getDuration() const;
	signed long long getStartTime() const;
	void setStartTime(signed long long aStartTime);
	unsigned long long getEndTime() const;
	void setEndTime(unsigned long long anEndTime);

private:
	unsigned long id;
	std::vector<Task> taskList;

	bool busy;
	unsigned long long duration;
	signed long long startTime;
	unsigned long long endTime;
};

#endif /* JOB_HPP_ */
