/*
 * Machine_test.cpp
 *
 *  Created on: 9 Oct 2016
 *      Author: Wessel
 */

#include "Machine.hpp"

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( Machine_Test )

BOOST_AUTO_TEST_CASE( MachineFreeOnCreationTest )
{
	Machine m1(0);

	BOOST_REQUIRE(m1.isFree());
}

BOOST_AUTO_TEST_CASE( MachineTimeOnDecrementDecreases )
{
	Machine m1(0);

	m1.setTask(60, 0);
	BOOST_REQUIRE(!m1.isFree());
	m1.decrementTime(15);

	BOOST_REQUIRE(!m1.isFree());
	BOOST_REQUIRE_EQUAL(45, m1.getTime());
}

BOOST_AUTO_TEST_CASE(MachineAssignmentOperator)
{
	Machine m1(0);
	Machine m2(1);

	m1.setTask(60, 0);
	m2 = m1;

	BOOST_REQUIRE_EQUAL(m1.getId(), m2.getId());
	BOOST_REQUIRE_EQUAL(m1.getTime(), m2.getTime());
	BOOST_REQUIRE_EQUAL(m1.getJobId(), m2.getJobId());
	BOOST_REQUIRE_EQUAL(m1.isFree(), m2.isFree());
}

BOOST_AUTO_TEST_CASE(MachineCopyConstructor)
{
	Machine m1(0);
	m1.setTask(60, 0);

	Machine m2(m1);

	BOOST_REQUIRE_EQUAL(m1.getId(), m2.getId());
	BOOST_REQUIRE_EQUAL(m1.getTime(), m2.getTime());
	BOOST_REQUIRE_EQUAL(m1.getJobId(), m2.getJobId());
	BOOST_REQUIRE_EQUAL(m1.isFree(), m2.isFree());
}

BOOST_AUTO_TEST_SUITE_END()
