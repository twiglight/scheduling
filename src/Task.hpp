/**
 * @file Task.hpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#ifndef TASK_HPP_
#define TASK_HPP_

#include <iostream>

/**
 * @brief A representation of a task of a job.
 */
class Task
{
public:
	Task();

	/**
	 * @brief A constructor which created a task with a given machine and time
	 *
	 * @param aId The id of the task
	 * @param aMachine The id of the machine which should handle this task
	 * @param aTime The time the task will take to complete
	 */
	Task(unsigned long aId, unsigned long aMachine, unsigned long aTime);

	Task(const Task& aTask);

	virtual ~Task();

	const Task& operator=(const Task& aTask);

	unsigned long getId() const;
	unsigned long getMachine() const;
	unsigned long getTime() const;

private:
	unsigned long id;
	unsigned long machine;
	unsigned long time;
};

#endif /* TASK_HPP_ */
