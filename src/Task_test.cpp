/*
 * Task_test.cpp
 *
 *  Created on: 9 Oct 2016
 *      Author: Wessel
 */

#include "Task.hpp"

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( Task_Test )

BOOST_AUTO_TEST_CASE( TaskAssignmentOperator )
{
	Task t1(0, 1, 60);
	Task t2(1, 0, 45);

	t2 = t1;

	BOOST_REQUIRE_EQUAL(t1.getId(), t2.getId());
	BOOST_REQUIRE_EQUAL(t1.getMachine(), t2.getMachine());
	BOOST_REQUIRE_EQUAL(t1.getTime(), t2.getTime());
}

BOOST_AUTO_TEST_CASE( TaskCopyConstructor )
{
	Task t1(0, 1, 60);
	Task t2(t1);

	BOOST_REQUIRE_EQUAL(t1.getId(), t2.getId());
	BOOST_REQUIRE_EQUAL(t1.getMachine(), t2.getMachine());
	BOOST_REQUIRE_EQUAL(t1.getTime(), t2.getTime());
}

BOOST_AUTO_TEST_SUITE_END()
