/**
 * @file Machine.hpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#ifndef MACHINE_HPP_
#define MACHINE_HPP_

#include <iostream>

/**
 * @brief A representation of a machine which handles tasks.
 */
class Machine
{
public:
	Machine();

	/**
	 * @brief A constructor which creates a machine with a given id.
	 *
	 * @param anId The id of the machine
	 */
	Machine(unsigned long anId);

	Machine(const Machine& aMachine);

	virtual ~Machine();

	const Machine& operator=(const Machine& aMachine);

	/**
	 * @brief Decrements the current task time with the given time.
	 *
	 * @param aTime The amount of time to decrement of the current task time
	 */
	void decrementTime(unsigned long aTime);

	/**
	 * @brief Checks if the machine is free based on the current (task)time it has.
	 *
	 * @return true if the machine is considered free, false if not
	 */
	bool isFree() const;

	/**
	 * @brief Sets a task in the machine, which is represented by a time and the id of the jobs its part of.
	 *
	 * @param aTime The time of the task
	 * @param aJobId The id of the job of which the task is part of
	 */
	void setTask(unsigned long aTime, unsigned long aJobId);

	unsigned long getId() const;
	unsigned long getTime() const;
	unsigned long getJobId() const;

private:
	unsigned long id;
	unsigned long time;
	unsigned long jobId;

};

#endif /* MACHINE_HPP_ */
