/**
 * @file JobShop.hpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#ifndef JOBSHOP_HPP_
#define JOBSHOP_HPP_

#include <string>
#include <iostream>
#include <vector>
#include <regex>
#include "Job.hpp"
#include "Machine.hpp"

/**
 * @brief A representation of a shop where tasks get allocated to machines.
 *
 * This class read the given file and converts this to machines, jobs and tasks
 * The JobShop contains the actual machines and jobs but sends the tasks to the jobs. The JobShop then schedules
 * tasks one by one in the machines until the jobs are completed.
 */
class JobShop
{
public:
	JobShop();

	virtual ~JobShop();

	/**
	 * @brief Reads a file and translates it to a vector of strings.
	 *
	 * @param aFileName The name of the file to be read.
	 *
	 * @throw std::invalid_argument Gets thrown if the file could not be opened
	 *
	 * @return A vector of strings which contains the file line by line
	 */
	std::vector<std::string> readFile(const char* aFileName);

	/**
	 * @brief Generates jobs, machines and tasks from the given file
	 *
	 * The file is read using regular expressions. The first line of the file
	 * should be the number of jobs and machines and after this line should contain
	 * the jobs and tasks thereof
	 *
	 * @param aFile A vector of strings which should contain a file line by line
	 *
	 * @exception std::length_error Thrown when the jobs and/or machine counts are smaller than defined
	 * @exception std::out_of_range Thrown when there are more jobs than defined in the first line
	 *
	 * @see Machine
	 * @see Job
	 * @see Task
	 */
	void fillJobList(std::vector<std::string> aFile);

	/**
	 * @brief Calls every scheduling function and prints the result.
	 *
	 * Here all scheduling functions are repeatedly called in a loop until all jobs are finished.
	 * Afterwards it calls the sortJobs function to prepare the #jobList for printing.
	 *
	 * First the jobs in jobList are sorted on their slack. Then tasks get allocated
	 * to machines, if possible. Afterwards the machineList is sorted based on the time of the tasks.
	 * Finally all the tasks times in the #machineList are decremented with the time of the
	 * shortest task and task which have no time left are completed.
	 */
	void scheduleJobs();

	const std::vector<Job>& getJobList() const;
	const std::vector<Machine>& getMachineList() const;

private:
	/**
	 * @brief Searches for the job with the longest duration (aka the critical path)
	 */
	unsigned long calculateCriticalPath();

	/**
	 * @brief Creates a machine with the given id and adds it to the #machineList
	 *
	 * Creates a new machine and adds it to the machineList ONLY when it is not
	 * already present. This is done so the machines do not have to have consecutive ids
	 *
	 * @param aMachineId The id for the new machine
	 */
	void createMachine(unsigned long aMachineId);

	/**
	 * @brief Schedules the tasks in the corresponding machines based on slack.
	 *
	 * The first step in the scheduling algorithm. Here the jobs in jobList are
	 * sorted on their slack. Afterwards tasks get allocated to machines, if possible.
	 *
	 * @param aCriticalPath The duration of the longest job, which is used to
	 * 	calculate the ls
	 *
	 * @see Job#calculateSlack
	 */
	void scheduleTasksInMachines(unsigned long aCriticalPath);

	/**
	 * @brief Finishes the shortest task currently in the #machineList and decrements the other tasks
	 *
	 * The second and last step in the scheduling algorithm. Here the machineList is sorted
	 * based on the time of the tasks. Afterwards the all the tasks times in the #machineList are
	 * decremented with the time of the shortest task and task which have no time left are completed.
	 */
	void handleTasksInMachines();

	/**
	 * @brief Sorts the jobs in #jobList based on job id.
	 */
	void sortJobs();

	std::vector<Job> jobList;

	std::vector<Machine> machineList;

	/**
	 * @brief The current time in the shop.
	 *
	 * This time is incremented each time a task is finished. With this time we can then
	 * determine start and end times for the jobs.
	 */
	unsigned long currentTime;

};
std::ostream& operator<<(std::ostream& os, const JobShop& aJobShop);

#endif /* JOBSHOP_HPP_ */
