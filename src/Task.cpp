/**
 * @file Task.cpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#include "Task.hpp"

Task::Task() :
		id(0), machine(0), time(0)
{
}

Task::Task(unsigned long aId, unsigned long aMachine, unsigned long aTime) :
		id(aId), machine(aMachine), time(aTime)
{
}

Task::Task(const Task& aTask) :
		id(aTask.id), machine(aTask.machine), time(aTask.time)
{
}

Task::~Task()
{
}

const Task& Task::operator =(const Task& aTask)
{
	if (this != &aTask)
	{
		id = aTask.id;
		machine = aTask.machine;
		time = aTask.time;
	}

	return *this;
}

unsigned long Task::getId() const
{
	return id;
}

unsigned long Task::getMachine() const
{
	return machine;
}

unsigned long Task::getTime() const
{
	return time;
}
