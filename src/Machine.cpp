/**
 * @file Machine.cpp
 *
 * @authors Agit Tunc (527782) & Wessel van Tunen (489881)
 */

#include "Machine.hpp"

Machine::Machine() :
		id(0), time(0), jobId(0)
{
}

Machine::Machine(unsigned long anId) :
		id(anId), time(0), jobId(0)
{
}

Machine::Machine(const Machine& aMachine) :
		id(aMachine.id), time(aMachine.time), jobId(aMachine.jobId)
{
}

Machine::~Machine()
{
}

const Machine& Machine::operator =(const Machine& aMachine)
{
	if (this != &aMachine)
	{
		id = aMachine.id;
		time = aMachine.time;
		jobId = aMachine.jobId;
	}

	return *this;
}

void Machine::decrementTime(unsigned long aTime)
{
	time -= aTime;
}

bool Machine::isFree() const
{
	return time <= 0;
}

void Machine::setTask(unsigned long aTime, unsigned long aJobId)
{
	time = aTime;
	jobId = aJobId;
}

unsigned long Machine::getId() const
{
	return id;
}

unsigned long Machine::getTime() const
{
	return time;
}

unsigned long Machine::getJobId() const
{
	return jobId;
}
